import flushPromises from 'flush-promises'
import { mount, createLocalVue } from '@vue/test-utils'
import MainForm from '@/components/MainForm.vue'
import VueFormulate from '@braid/vue-formulate'

describe('MainForm.vue - DOM elements', () => {
  const localVue = createLocalVue()
  localVue.use(VueFormulate)
  const wrapper = mount(MainForm, { localVue })

  it('form is rendered', () => {
    expect(wrapper.exists()).toBe(true)
  })

  it('all form fields are rendered', () => {
    const h2 = wrapper.findComponent('.form-title')
    const formInputs = wrapper.findAllComponents('.formulate-input')
    const formStatus = wrapper.findComponent('.form-status')
    expect(h2.exists()).toBe(true)
    expect(formInputs.length).toBe(5)
    expect(formStatus.exists()).toBe(true)
  })

  it('has disabled submit button if no fields are populated', async () => {
    const submitButton = wrapper.findComponent('.form-submit button[type="submit"]')
    expect(submitButton.exists()).toBe(true)
    expect(submitButton.attributes('disabled')).toBe('disabled')
  })
})

describe('MainForm.vue - user actions', () => {
  const localVue = createLocalVue()
  localVue.use(VueFormulate)
  let wrapper = mount(MainForm, { localVue })

  it('properly takes user inputs', () => {
    const inputsAttributeAndValue = {
      '.formulate-input input[name="name"]': 'testing',
      '.formulate-input input[name="email"]': 'email@email.email',
      '.formulate-input input[name="subject"]': 'test subject',
      '.formulate-input textarea[name="message"]': 'test message'
    }

    for (const [attr, value] of Object.entries(inputsAttributeAndValue)) {
      const input = wrapper.find(attr)
      input.setValue(value)
      expect(input.element.value).toBe(value)
    }
  })

  it('displays validation errors upon invalid user input', async () => {
    const allFormInputs = wrapper.findAll('.formulate-input')

    // index 2 is not listed below because that input is optional, so we can skip its check
    const inputIndexAndType = {
      0: 'input[name="name"]',
      1: 'input[name="email"]',
      3: 'textarea[name="message"]'
    }


    for (const [index, inputAttribute] of Object.entries(inputIndexAndType)) {
      const input = allFormInputs.at(index).find(inputAttribute)
      input.setValue('')
      expect(input.element.value).toBe('')
      input.trigger('blur')
    }

    await flushPromises()

    const inputIndexAndErrorMessage = {
      0: 'Name is required.Name must be at least 5 characters long.',
      1: 'Email is required.Please enter a valid email address.',
      3: 'Message is required.'
    }

    for (const [index, message] of Object.entries(inputIndexAndErrorMessage)) {
      const inputError = allFormInputs.at(index).find('.formulate-input-errors')
      expect(inputError.exists()).toBe(true)
      expect(inputError.text()).toContain(message)
    }
  })

  it('sends form data if validation is passed', async () => {
    const mockFn = jest.fn()
    const $axios = { post: (url, payload) => mockFn(url, payload) }

    wrapper = mount(MainForm, {
      localVue,
      mocks: {
        $axios
      }
    })

    const inputsAttributeAndValue = {
      '.formulate-input input[name="name"]': 'testing',
      '.formulate-input input[name="email"]': 'email@email.email',
      '.formulate-input input[name="subject"]': 'test subject',
      '.formulate-input textarea[name="message"]': 'test message'
    }

    for (const [attr, value] of Object.entries(inputsAttributeAndValue)) {
      const input = wrapper.find(attr)
      input.setValue(value)
      expect(input.element.value).toBe(value)
      input.trigger('blur')
    }

    await flushPromises()

    const submitButton = wrapper.findComponent('.form-submit button[type="submit"]')
    expect(submitButton.exists()).toBe(true)
    expect(submitButton.attributes('disabled')).toBe(undefined)
    await submitButton.trigger('click')

    // TBD: VueFormulate seems to need a workaround for form submission tests
    // because it doesn't properly trigger submit event on submit button click during tests
    // expect(wrapper.vm.success).toBe(true)
    // expect(mockFn).toHaveBeenCalled()
    // expect(mockFn).toHaveBeenCalledWith(url, payload)

    mockFn.mockRestore()
  })
})
