# vue-api

This is a simple Email Form project written in Vue.js. Apart from basic interface it uses Vue Formulate library for form building, data handling and validation. In order to launch the project locally, be sure to complete the first two steps that are specified below in their respective order.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
